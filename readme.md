# Pangolin OS
A 16 bit operating system written in FASM

# Goals
- [ ] Implement basic file I/O with Unix like file descriptors that supports 0 STDIN, 1 STDOUT, 2 STDERR
- [x] Create a tight memory layout with kernel and FAT + root entries cache in segment 0x0000
- [ ] Implement an Atari2600 emulator
- [ ] Add 32 bit & 64 bit modes
- [ ] Port FASM