format binary as 'img'
use16
ORG 0x0100

include '../kernel_api.inc'

start:
    xor bx, bx
    xor si, si
    xor cx, cx

    .skip_file:
    mov al, [bx]
    inc bx
    test al, al
    jz .count
    cmp al, ' '
    je .count
    jmp .skip_file

    .count:
    mov al, [bx + si]
    test al, al
    jz .print
    inc si
    inc cx
    jmp .count

    .print:
    mov ah, KERNEL_TTY_PRINT
    kernel_int
    mov bx, newline
    mov cx, newline.len
    kernel_int

    xor ah, ah
    kernel_int

newline: db 13, 10
.len = $ - newline