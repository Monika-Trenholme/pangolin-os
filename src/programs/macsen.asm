format binary as 'img'
use16
ORG 0x0100

include '../kernel_api.inc'

start:
    mov bx, message
    mov cx, message.len
    mov ah, KERNEL_TTY_PRINT
    kernel_int

    xor ah, ah
    kernel_int

message:
    db "does neb yn ei nabod o;", 13, 10
    db "mae mil a chwe chant o flynyddoedd,", 13, 10
    db "yn amser rhy hir i'r co';", 13, 10
    db "pan aeth Magnus Maximus o Cymru", 13, 10
    db "yn y flwyddyn tri-chant-wyth-tri", 13, 10
    db "a'n gadael yn genedl gyfan", 13, 10
    db "a heddiw: Wele ni!", 13, 10
    times 2 db "Ry'n ni yma o hyd.", 13, 10
    db "Er gwaetha pawb a phopeth,", 13, 10
    db "er gwaetha pawb a phopeth,", 13, 10
    db "er gwaetha pawb a phopeth:", 13, 10
    db "Ry'n ni yma o hyd!", 13, 10
.len = $ - message