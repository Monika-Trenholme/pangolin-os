format binary as 'img'
use16
ORG 0x0100

include '../kernel_api.inc'

start:
    mov ah, KERNEL_SHUTDOWN
    kernel_int

    .error:
    mov ah, KERNEL_TTY_PRINT
    mov bx, err_msg
    mov cx, err_msg.len
    kernel_int

    xor ah, ah
    kernel_int

err_msg: db "Shutdown failed", 13, 10
.len = $ - err_msg