format binary as 'img'
use16
ORG 0x0100

include '../kernel_api.inc'

DIR_OFFSET = 0x200
DIR_BUFFER_SIZE = 0x4000

start:
    ; Open root dir
    xor bx, bx
    mov ah, KERNEL_OPEN_FILE
    kernel_int

    .read:
    ; Fill read buffer
    mov bx, DIR_OFFSET
    mov cx, DIR_BUFFER_SIZE
    mov ah, KERNEL_READ_FILE
    kernel_int

    .l:
    mov cl, [bx]
    test cl, cl
    jz .quit
    
    mov cx, 11
    mov ah, KERNEL_TTY_PRINT
    kernel_int

    push bx
    mov bx, new_line
    mov cx, new_line.len
    kernel_int
    pop bx

    add bx, 32
    cmp bx, DIR_OFFSET + DIR_BUFFER_SIZE
    jb .l
    jmp .read

    .quit:
    ; Close root dir
    mov ah, KERNEL_CLOSE_FILE
    kernel_int

    ; Quit program
    xor ah, ah
    kernel_int

new_line: db 13, 10
.len = $ - new_line