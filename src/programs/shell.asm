format binary as 'img'
use16
ORG 0x0100

include '../kernel_api.inc'

start:
    ; Print message
    mov bx, message
    mov cx, message.len
    mov ah, KERNEL_TTY_PRINT
    kernel_int

    .command:
    ; Print prompt
    mov bx, prompt
    mov cx, prompt.len
    mov ah, KERNEL_TTY_PRINT
    kernel_int

    xor dx, dx

    .type:
    ; Get key input
    mov ah, KERNEL_TTY_KEY
    kernel_int
    jz .type

    ; If enter key, run command buffer
    cmp al, 13
    je .run

    ; If back space clear last character
    cmp al, 8
    jne .store
    cmp dl, 0
    je .type
    mov bx, backspace
    mov cx, backspace.len
    mov ah, KERNEL_TTY_PRINT
    kernel_int
    dec dl
    jmp .type

    .store:
    ; Store key
    mov bx, command_buffer
    add bx, dx
    mov [bx], al
    ; Print key
    mov cx, 1
    mov ah, KERNEL_TTY_PRINT
    kernel_int
    inc dl
    jmp .type

    .run:
    ; Println
    mov bx, new_line
    mov cx, new_line.len
    mov ah, KERNEL_TTY_PRINT
    kernel_int

    ; Run command
    mov bx, command_buffer
    add bx, dx
    mov [bx], word 0
    mov bx, command_buffer
    mov ah, KERNEL_START_PROCESS
    kernel_int
    jnc .command

    ; Print error message
    mov bx, error
    mov cx, error.len
    mov ah, KERNEL_TTY_PRINT
    kernel_int
    jmp .command

message: db 'Welcome to Pangolin OS', 13, 10
.len = $ - message

prompt: db '> '
.len = $ - prompt

error: db 'Unable to run command', 13, 10
.len = $ - error

new_line: db 13, 10
.len = $ - new_line

backspace: db 8, 32, 8
.len = $ - backspace

command_buffer: times 256 db 0