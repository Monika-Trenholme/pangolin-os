; Memory layout in segment 0x0000
SECTOR_SIZE       = 512
BOOT_SECTOR       = 0x7C00
BOOT_LOADER       = 0xC500
BOOT_LOADER_SECTS = 2
KERNEL            = 0xC900
FAT               = 0x0500
FAT_ROOT_ENTRIES  = 0x8500

; FAT constants
FAT_DIR_ENTRY_SIZE    = 32
FAT_CLUSTER_LOW       = 26

; BIOS interrupts
VIDEO_INT = 0x10
TTY_OUT   = 0x0E

DISK_INT        = 0x13
DISK_READ       = 0x02
DISK_PARAMETERS = 0x08


; Structs
include 'macros.inc'

struct BPB
    jmp .skip
    nop
    .oem_label         dq 'PANGOLIN'
    .sector_size       dw 512
    .sects_per_cluster db 0
    .reserved_sects    dw 0
    .fat_count         db 0
    .root_dir_entries  dw 0
    .sector_count      dw 0
    .media_description db 0
    .sects_per_fat     dw 0
    .sects_per_track   dw 18
    .heads_count       dw 2
    .hidden_sects      dd 0
    .sect_count_big    dd 0
    .drive_num         db 0
    .reserved          db 0
    .signature         db 0
    .volume_id         dd 0
    .volume_label      db 'PANGOLIN OS'
    .filesystem_type   dq 0
    .skip:
ends BPB

PARTITION_TABLE      = 0x1BE
BOOT_DISABLE         = 0
BOOT_ENABLE          = 0x80
struct PARTITION boot_flag, start_head, start_cs, system_id, end_head, end_cs, offset, size
    .boot_flag  db boot_flag+0
    ; cylinder = 1023, head = 255, sector = 63 for invalid address
    .start_chs:
        db start_head+0
        dw start_cs+0
    .system_id  db system_id+0
    ; cylinder = 1023, head = 255, sector = 63 for invalid address
    .end_chs:
        db end_head+0
        dw end_cs+0
    ; Starting LBA address
    .lba_offset dd offset+0
    .lba_size   dd size+0
ends PARTITION