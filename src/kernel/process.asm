; Number of segments used by processes
process_used_segments: db 0
pid: db 0

; Quit process
quit_process_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    ; Decrement register pointer and used segments
    sub [regs_pointer], word sizeof.REGS
    dec word [process_used_segments]
    mov bx, [regs_pointer]
    and [.regs.f], word not CA_FLAG
    ret

; Start process
; Arguments:
;   ds:bx: 256 byte command string buffer
start_process_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    mov bp, sp
    sub sp, 12
    mov di, sp

    mov ax, [.regs.ds]
    mov bx, [.regs.bx]
    mov ds, ax
    call fat_parse_name
    xor ax, ax
    mov ds, ax

    mov ax, [bpb.root_dir_entries]
    mov bx, FAT_ROOT_ENTRIES
    mov si, di
    call fat_search_file
    jc .err

    ; Allocate segment and read program file into it
    mov ax, [bx + FAT_CLUSTER_LOW]
    inc word [process_used_segments]
    mov bx, [process_used_segments]
    shl bx, 12
    mov es, bx
    mov bx, 0x0100
    call fat_read_file
    jc .err

    mov bx, [regs_pointer]

    mov ax, [.regs.ds]
    mov si, [.regs.bx]
    mov di, 0
    mov ds, ax
    mov cx, 256
    repe movsb
    xor ax, ax
    mov ds, ax

    ; Increment register pointer
    add [regs_pointer], word sizeof.REGS

    ; Prepare ds, es, and ss:sp
    mov ax, es
    mov ds, ax
    mov ss, ax
    xor sp, sp

    ; Jump into program
    push ax
    push word 0x0100
    retf

    .err:
    mov bx, [regs_pointer]
    or [.regs.f], word CA_FLAG
    mov sp, bp
    ret