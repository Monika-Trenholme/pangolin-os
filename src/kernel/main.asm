format binary as 'img'
use16
ORG KERNEL
MAX_PROCESSES = 8

include '../boot_common.inc'

; Virtual bpb struct at boot loader offset
virtual at BOOT_LOADER
    bpb BPB
end virtual

jmp start

include '../boot_common.asm'
include 'interrupt_handler.asm'
include 'fat.asm'
include 'file.asm'
include 'process.asm'

interrupt_table:
    dw quit_process_interrupt
    dw start_process_interrupt
    dw tty_print_interrupt
    dw tty_key_interrupt
    dw open_file_interrupt
    dw close_file_interrupt
    dw read_file_interrupt
    dw shutdown_interrupt
MAX_INTERRUPT = ($ - interrupt_table) / 2 - 1

kernel_msg: db 'Kernel loaded...', 13, 10
.len = $ - kernel_msg

shell_command: db 'shell'
times 256 - ($ - shell_command) db 0

start:
    mov [fat_start_sector], ax

    mov ax, kernel_msg.len
    mov bx, kernel_msg
    call print_string

    mov ax, SECTOR_SIZE
    xor bx, bx
    mov bl, [bpb.sects_per_cluster]
    mul bx
    mov [fat_cluster_size], ax

    ; Load interrupt handler into index 0x20 of IVT
    mov [0x20 * 4], word interrupt_handler
    mov [0x20 * 4 + 2], cs

    .shell:
    mov ah, 1
    mov bx, shell_command
    int 0x20
    jc error.shell
    jmp .shell

error:
    .shell_msg: db 'Kernel error: failed to launch shell'
    .shell_msg_len = $ - .shell_msg
    .shell:
    mov ax, .shell_msg_len
    mov bx, .shell_msg
    call print_string
    jmp .hault

    .hault:
    hlt
    jmp .hault

; Print to tty
; Arguments:
;   ds:bx: string buffer
;   cx: string len
tty_print_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    mov ax, [.regs.cx]
    mov cx, [.regs.bx]
    mov bx, [.regs.ds]
    mov ds, bx
    mov bx, cx
    call print_string
    ret

; Get tty key input
; Return:
;   z flag: set on no key
;   ah: scan code
;   al: ascii
tty_key_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    mov ah, 1
    int 0x16
    jnz .key

    .no_key:
    or [.regs.f], word Z_FLAG
    ret

    .key:
    xor ah, ah
    int 0x16
    and [.regs.f], word not Z_FLAG
    mov [.regs.ax], ax
    ret

; Shutdown interrupt
; Uses APM, should replace with ACPI, implementation from OSDev.org
shutdown_interrupt:
    ; Check installation
    mov ah, 53h ; this is an APM command
    mov al, 00h ; installation check command
    xor bx, bx  ; device id (0 = APM BIOS)
    int 15h     ; call the BIOS function through interrupt 15h
    jc .error   ; if the carry flag is set there was an error
                ; the function was successful
                    ; AX = APM version number
                        ; AH = Major revision number (in BCD format)
                        ; AL = Minor revision number (also BCD format)
                    ; BX = ASCII characters "P" (in BH) and "M" (in BL)
                    ; CX = APM flags (see the official documentation for more details)
    
    ; Check that APM version is >= 1.1
    cmp ax, (0x01 shl 8) + 0x01
    jb .error

    ; disconnect from any APM interface
    mov ah,53h ;this is an APM command
    mov al,04h ;interface disconnect command
    xor bx,bx  ;device id (0 = APM BIOS)
    int 15h    ;call the BIOS function through interrupt 15h
    setc al
    cmp ah, 03h ; if ah == 03 then no interface was connected
    setne ah
    test al, ah
    jnz .error

    ; Connect to the real mode interface
    mov ah, 53h ; this is an APM command
    mov al, 01h ; real mode interface id
    xor bx, bx  ; device id (0 = APM BIOS)
    int 15h     ; call the BIOS function through interrupt 15h
    jc .error   ; if the carry flag is set there was an error

    ; Set APM version
    mov ah, 53h   ;this is an APM command
    mov al, 0eh   ;set driver supported version command
    mov bx, 0000h ;device ID of system BIOS
    mov ch, 01h   ;APM driver major version
    mov cl, 01h   ;APM driver minor version (can be 01h or 02h if the latter one is supported)
    int 15h
    jc .error

    ; Check APM version again
    cmp ax, (0x01 shl 8) + 0x01
    jl .error

    ;Enable power management for all devices
    mov ah, 53h   ;this is an APM command
    mov al, 08h   ;Change the state of power management...
    mov bx, 0001h ;...on all devices to...
    mov cx, 0001h ;...power management on.
    int 15h       ;call the BIOS function through interrupt 15h
    jc .error     ;if the carry flag is set there was an error

    ;Set the power state for all devices
    mov ah, 53h    ;this is an APM command
    mov al, 07h    ;Set the power state...
    mov bx, 0001h  ;...on all devices to...
    mov cx, 03h    ;see above
    int 15h        ;call the BIOS function through interrupt 15h

    .error:
    mov bx, [regs_pointer]
    virtual at bx
        .regs REGS
    end virtual
    or [.regs.f], word CA_FLAG
    ret