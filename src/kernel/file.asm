; File descriptor table
FD_SIZE            = 2 * 2
FD_SIZE_LOG        = 2
FD_CURRENT_CLUSTER = 0
FD_CURRENT_OFFSET  = 2
fd_table: times 256 * FD_SIZE db 0

; Open file
; Arguments:
;   ds:bx: file name, unless bx is 0, then open root directory
; Return:
;   al: file descriptor
open_file_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    ; Setup stack space to store parsed file name
    mov bp, sp
    sub sp, 12
    mov di, sp

    ; If bx is 0 set cluster to 0x8000 indicating root directory
    mov bx, [.regs.bx]
    cmp bx, 0
    jne .parse_name
    mov dx, 0x8000
    jmp .get_fd

    ; Parse file name
    .parse_name:
    mov ax, [.regs.ds]
    mov ds, ax
    call fat_parse_name
    xor ax, ax
    mov ds, ax

    ; Search for file in root directory
    mov ax, [bpb.root_dir_entries]
    mov bx, FAT_ROOT_ENTRIES
    mov si, di
    call fat_search_file
    jc .error

    ; Set cluster to first cluster of file
    mov dx, [bx + FAT_CLUSTER_LOW]

    ; Search for available file descriptor
    .get_fd:
    xor al, al
    mov si, fd_table
    .search:
    ; If there is no available file descriptor return with error
    cmp si, fd_table + 256 * FD_SIZE
    je .error
    ; If the cluster is zeroed then the file descriptor is available
    cmp [si + FD_CURRENT_CLUSTER], word 0
    je .set_fd
    inc al
    add si, FD_SIZE
    jmp .search

    ; Set file descriptor with initial values and return it in al
    .set_fd:
    mov [si + FD_CURRENT_CLUSTER], dx
    mov [si + FD_CURRENT_OFFSET], word 0
    mov bx, [regs_pointer]
    mov cx, [.regs.ax]
    mov cl, al
    mov [.regs.ax], cx
    jmp .return

    .error:
    mov bx, [regs_pointer]
    or [.regs.f], word CA_FLAG

    .return:
    mov sp, bp
    ret

; Close file interrupt
; Arguments:
;   al: file descriptor
close_file_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    ; Get address of file descriptor
    mov ax, [.regs.ax]
    xor ah, ah
    shl ax, FD_SIZE_LOG
    mov si, fd_table
    add si, ax

    ; Zero file descriptor cluster to mark as available
    mov [si + FD_CURRENT_CLUSTER], word 0
    ret

; Read file interrupt
; Arguments:
;   al: file descriptor
;   es:bx: buffer
;   cx: byte count
read_file_interrupt:
    virtual at bx
        .regs REGS
    end virtual

    ; Get address of file descriptor
    mov ax, [.regs.ax]
    xor ah, ah
    shl ax, FD_SIZE_LOG
    mov si, fd_table
    add si, ax
    ; Get file pointer's current cluster and offset
    mov ax, [si + FD_CURRENT_CLUSTER]
    mov dx, [si + FD_CURRENT_OFFSET]
    ; Save pointer to file descriptor
    push si
    ; If current cluster = 0x8000 then read root dir
    cmp ax, 0x8000
    je .root

    ; Reading from actual file not implemented yet :p
    jmp .return

    .root:
    ; Copy words
    mov si, FAT_ROOT_ENTRIES
    add si, dx
    mov es, [.regs.es]
    mov di, [.regs.bx]
    mov cx, [.regs.cx]
    shr cx, 1
    repe movsw

    ; Copy byte if odd
    test [.regs.cx], word 1
    jz .return
    movsb

    .return:
    ; Advance file pointer
    add dx, [.regs.cx]
    pop si
    mov [si + FD_CURRENT_OFFSET], dx
    ret