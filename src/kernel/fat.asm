FAT_DIR_ENTRY_SIZE    = 32
FAT_CLUSTER_LOW       = 26

fat_start_sector: dw 0 ; Starting sector for file data
fat_cluster_size: dw 0 ; Size of cluster in bytes

; TODO: Implement extensions and sub directories
; Parse name for FAT16 file
; Arguments:
;   ds:bx: input buffer
;   es:di: output buffer
fat_parse_name:
    push ax
    push bx
    push cx
    push di

    xor cl, cl

    .parse:
    cmp cl, 11
    je .end
    mov al, [bx]
    test al, al
    jz .pad
    cmp al, ' '
    je .pad

    ; If al is lowercase capitalise it
    cmp al, 'a'
    jb .not_lower
    cmp al, 'z'
    ja .not_lower
    ; 'a' - 'A' = 32
    sub al, 32
    .not_lower:

    ; Write to output and inc
    mov [es:di], al
    inc bx
    inc di
    inc cl
    jmp .parse

    ; Pad with spaces
    .pad:
    cmp cl, 11
    je .end
    mov [es:di], byte ' '
    inc di
    inc cl
    jmp .pad

    .end:
    pop di
    pop cx
    pop bx
    pop ax
    ret

; Search for file in dir buffer
; Arguments:
;   ax: entry count
;   bx: dir buffer
;   si: file name
; Returns:
;   c flag: set when not found
;   bx: pointer to file
fat_search_file:
    push ax
    push cx
    push di

    .search:
    mov di, bx
    push si
    mov cx, 11
    repe cmpsb
    pop si
    clc
    je .end

    add bx, FAT_DIR_ENTRY_SIZE
    dec ax
    jnz .search

    stc

    .end:
    pop di
    pop cx
    pop ax
    ret

; Read cluster to memory
; Arguments:
;   ax: cluster
;   es:bx: buffer
; Return:
;   c flag: set on error
fat_read_cluster:
    push ax
    push cx
    push dx

    ; Calculate sector for cluster
    ; sector = (cluster - 2) * sectors / cluster + start sector
    xor cx, cx
    mov cl, [bpb.sects_per_cluster]
    sub ax, 2
    mul cx
    add ax, [fat_start_sector]

    ; Read cluster
    call lba_to_chs
    mov ax, cx
    call read_disk_chs

    pop dx
    pop cx
    pop ax
    ret

; Read file to memory
; Arguments:
;   ax: first cluster
;   es:bx: buffer
; Return:
;   c flag: set on error
fat_read_file:
    push ax
    push bx
    push si

    mov si, ax

    .read:
    mov ax, si
    call fat_read_cluster
    jc .end

    ; Advance buffer pointer
    add bx, [fat_cluster_size]

    ; Get next cluster
    shl si, 1
    mov si, [FAT + si]
    cmp si, 0xFFF8
    jb .read

    clc
    .end:
    pop si
    pop bx
    pop ax
    ret