; Register struct
struct REGS
    .ax dw ?
    .bx dw ?
    .cx dw ?
    .dx dw ?
    .si dw ?
    .di dw ?
    .sp dw ?
    .bp dw ?
    .ds dw ?
    .es dw ?
    .fs dw ?
    .gs dw ?
    .ss dw ?
    .f  dw ?
ends REGS

; Space to backup registers from interrupts
regs: times MAX_PROCESSES * sizeof.REGS db 0
regs_pointer: dw regs

CA_FLAG = 0x0001
PA_FLAG = 0x0004
AX_FLAG = 0x0010
Z_FLAG  = 0x0040
S_FLAG  = 0x0080
TR_FLAG = 0x0100
IE_FLAG = 0x0200
DI_FLAG = 0x0400
OF_FLAG = 0x0800

interrupt_handler:
    ; Save ds:bx to stack
    push ds
    push bx
    
    ; Zero ds
    xor bx, bx
    mov ds, bx
    
    ; Back up registers
    mov bx, [regs_pointer]
    virtual at bx
        .regs REGS
    end virtual

    mov [.regs.ax], ax
    pop ax
    mov [.regs.bx], ax
    mov [.regs.cx], cx
    mov [.regs.dx], dx

    mov [.regs.si], si
    mov [.regs.di], di

    pop ax
    mov [.regs.ds], ax
    mov [.regs.es], es
    mov [.regs.fs], fs
    mov [.regs.gs], gs
    mov [.regs.ss], ss

    mov [.regs.sp], sp
    mov [.regs.bp], bp

    mov bp, sp
    mov ax, [ss:bp + 4]
    mov [.regs.f], ax

    ; Zero all segment registers
    xor ax, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    ; Set stack to FFFA, this leaves room for the kernel to call an initial interrupt for the shell
    mov sp, 0xFFFA
    mov bp, sp

    ; Check if interrupt is valid
    mov ax, [.regs.ax]
    cmp ah, MAX_INTERRUPT
    jbe .valid_interrupt

    or [.regs.f], word CA_FLAG
    jmp .return
    
    .valid_interrupt:
    ; Call interrupt
    xor al, al
    shr ax, 7
    add ax, interrupt_table
    mov si, ax
    mov ax, [si]
    call ax

    .return:
    ; Reset ds:bx
    xor ax, ax
    mov ds, ax
    mov bx, [regs_pointer]

    ; Restore stack
    mov ss, [.regs.ss]
    mov sp, [.regs.sp]
    mov bp, [.regs.bp]

    ; Set flags
    mov ax, [.regs.f]
    mov di, sp
    mov [ss:di + 4], ax

    ; Put ds:bx on stack
    push word [.regs.ds]
    push word [.regs.bx]

    ; Restore registers
    mov ax, [.regs.ax]
    mov cx, [.regs.cx]
    mov dx, [.regs.dx]

    mov si, [.regs.si]
    mov di, [.regs.di]

    mov es, [.regs.es]
    mov fs, [.regs.fs]
    mov gs, [.regs.gs]

    ; Pop off ds:bx
    pop bx
    pop ds

    iret