; Print string to TTY
; Arguments:
;   ax: String size
;   bx: String address
print_string:
    push ax
    push bx
    push cx
    mov cx, ax
    mov ah, TTY_OUT
    
    .l:
    cmp cx, 0
    je .end
    mov al, [bx]
    int VIDEO_INT
    inc bx
    dec cx
    jmp .l
    
    .end:
    pop cx
    pop bx
    pop ax
    ret

; Print word sized number to TTY
; Arguments:
;   ax: number to print
print_int:
    push ax
    push bx
    push cx
    push dx
    push bp

    mov bp, sp
    sub sp, 6
    mov bx, bp

    mov cx, 10
    .divide:
    xor dx, dx
    div cx
    add dl, '0'
    dec bx
    mov [bx], dl
    cmp ax, 0
    jne .divide

    mov ah, TTY_OUT
    .print:
    mov al, [bx]
    int VIDEO_INT
    inc bx
    cmp bx, bp
    jne .print

    mov sp, bp

    pop bp
    pop dx
    pop cx
    pop bx
    pop ax
    ret

; Print carriage return and newline to TTY
println:
    push ax

    mov ah, TTY_OUT
    mov al, 13
    int VIDEO_INT
    mov al, 10
    int VIDEO_INT

    pop ax
    ret

; Converts an LBA address to a CHS address
; Arguments:
;   ax: LBA address
; Returns:
;   cx [bits 0-5]: sector number
;   cx [bits 6-15]: cylinder
;   dh: head
lba_to_chs:
    push ax
    push dx

    xor dx, dx                     ; dx = 0
    div word [bpb.sects_per_track] ; ax = LBA / SectorsPerTrack
                                   ; dx = LBA % SectorsPerTrack

    inc dx                         ; dx = (LBA % SectorsPerTrack + 1) = sector
    mov cx, dx                     ; cx = sector

    xor dx, dx                     ; dx = 0
    div word [bpb.heads_count]     ; ax = (LBA / SectorsPerTrack) / Heads = cylinder
                                   ; dx = (LBA / SectorsPerTrack) % Heads = head
    mov dh, dl                     ; dh = head
    mov ch, al                     ; ch = cylinder (lower 8 bits)
    shl ah, 6
    or cl, ah                      ; put upper 2 bits of cylinder in CL

    pop ax
    mov dl, al                     ; restore DL
    pop ax
    ret

; Reads sectors from disk using a CHS address
; Arguments:
;   al: number of sectors
;   es:bx: buffer
;   cx: sector and cylinder
;   dh: head
; Returns:
;   c flag: set on error
;   ah: status on error
read_disk_chs:
    push dx
    push ax

    mov ah, DISK_READ
    mov dl, [bpb.drive_num]
    clc
    int DISK_INT

    pop dx
    mov al, dl ; Restore al
    pop dx
    ret