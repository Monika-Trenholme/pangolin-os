macro struct [def]
{
    common
    struc def {
}

macro struct_helper name
{
    virtual at 0
        name name
        sizeof.#name = $ - name
        name equ sizeof.#name
    end virtual
}

ends fix } struct_helper