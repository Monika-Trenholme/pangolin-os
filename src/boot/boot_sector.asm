format binary as 'img'
use16
ORG BOOT_SECTOR

include '../boot_common.inc'

bpb BPB

start:
    cli
    cld
    jmp 0x0000:init

include '../boot_common.asm'

disk_geom_msg: db 'Disk geometry: '
.len = $ - disk_geom_msg

by_string: db ' x '
.len = $ - by_string

err_string: db 'Boot sector error: '
.len = $ - err_string

init:
    ; Setup tiny memory model
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    mov sp, ax
    mov bp, sp

    ; Print volume label
    mov ax, 11
    mov bx, bpb.volume_label
    call print_string
    call println

    ; Drives above 0x8f are dubious
    cmp dl, 0x8f
    ja error.disk_num

    ; Store drive number
    mov [bpb.drive_num], dl

    ; Get disk parameters
    mov ah, DISK_PARAMETERS
    xor di, di ; apparently guards against BIOS bugs
    int DISK_INT

    ; Store head count
    mov dl, dh
    xor dh, dh
    inc dx
    mov [bpb.heads_count], dx

    ; Store sects/track
    and cl, 0x3f
    xor ch, ch
    mov [bpb.sects_per_track], cx

    ; Print disk geometry
    xor ax, ax
    mov al, disk_geom_msg.len
    mov bx, disk_geom_msg
    call print_string
    mov ax, dx
    call print_int
    mov al, by_string.len
    mov bx, by_string
    call print_string
    mov ax, cx
    call print_int
    mov al, by_string.len
    mov bx, by_string
    call print_string
    mov ax, SECTOR_SIZE
    call print_int
    call println

    ; Read boot loader
    mov ax, 1
    call lba_to_chs
    mov ax, BOOT_LOADER_SECTS
    mov bx, BOOT_LOADER
    call read_disk_chs
    jc error.disk_read

    ; Jump to boot loader
    jmp BOOT_LOADER

error:
    .prefix: db "Boot sector error: "
    .prefix_len = $ - .prefix

    .disk_num_msg: db "Disk number"
    .disk_num_msg_len = $ - .disk_num_msg
    .disk_num:
    mov bx, .prefix
    mov ax, .prefix_len
    call print_string
    mov bx, .disk_num_msg
    mov ax, .disk_num_msg_len
    call print_string
    jmp .hault

    .disk_read_msg: db "Disk read"
    .disk_read_msg_len = $ - .disk_read_msg
    .disk_read:
    mov bx, .prefix
    mov ax, .prefix_len
    call print_string
    mov bx, .disk_read_msg
    mov ax, .disk_read_msg_len
    call print_string

    .hault:
    hlt
    jmp .hault

    ; Pad sector
    times PARTITION_TABLE - ($ - $$) db 0
    ; Make partition 1 bootable, FAT16, sector offset 1, and 32 Mib - 512b
    .1 PARTITION BOOT_ENABLE, 0xFF, 0xFFFF, 0x04, 0xFF, 0xFFF, 1, 32 * 1024 * 1024 / SECTOR_SIZE - 1
    .2 PARTITION
    .3 PARTITION
    .4 PARTITION
boot_signature: db 0x55, 0xaa