format binary as 'img'
use16
ORG BOOT_LOADER

include '../boot_common.inc'

; BPB struct from boot sector
virtual at BOOT_SECTOR
    mbr_bpb BPB
end virtual

; BPB stub should not be written to disk to maintain file system integrity
bpb BPB
jmp start

include '../boot_common.asm'

run_msg: db "Boot loader running...", 13, 10
.len = $ - run_msg

fat_size_msg: db 'FAT size: '
.len = $ - fat_size_msg

by_string: db ' x '
.len = $ - by_string

root_dir_entries_msg: db 'Root dir entries: '
.len = $ - root_dir_entries_msg

kernel_file_name: db 'KERNEL     '

fats_size:      dw 0 ; size of FATs in sectors
root_dir_size:  dw 0 ; size of root dir in sectors
start_sector:   dw 0 ; start sector for file data

; Apparently to be recognised the FAT file system needs a valid VBR
times 510 - ($ - $$) db 0
db 0x55, 0xaa

start:
    mov ax, run_msg.len
    mov bx, run_msg
    call print_string

    ; Copy disk geometry from boot sector bpb
    mov ax, [mbr_bpb.heads_count]
    mov [bpb.heads_count], ax
    mov ax, [mbr_bpb.sects_per_track]
    mov [bpb.sects_per_track], ax

    ; Calculate size of FATS and print it
    mov ax, fat_size_msg.len
    mov bx, fat_size_msg
    call print_string
    xor ax, ax
    mov al, [bpb.fat_count]
    call print_int
    mov cx, ax
    mov ax, by_string.len
    mov bx, by_string
    call print_string
    mov ax, [bpb.sects_per_fat]
    call print_int
    call println
    mul cx
    ; Store FATs sector count
    mov [fats_size], ax

    ; Read primary FAT
    mov bx, FAT
    mov ax, [bpb.reserved_sects]
    inc ax
    call lba_to_chs
    mov ax, [bpb.sects_per_fat]
    call read_disk_chs
    jc error.disk_read

    ; Print number of root dir entries
    mov ax, root_dir_entries_msg.len
    mov bx, root_dir_entries_msg
    call print_string
    mov ax, [bpb.root_dir_entries]
    call print_int
    call println

    ; Get number of sects for root dir entries
    ; sectors = (entry count * entry size + sector size - 1) / sector size
    mov bx, FAT_DIR_ENTRY_SIZE
    mul bx
    xor dx, dx
    add ax, SECTOR_SIZE
    dec ax
    mov bx, SECTOR_SIZE
    div bx
    mov [root_dir_size], ax

    ; Read root dir entries
    mov ax, [fats_size]
    add ax, [bpb.reserved_sects]
    inc ax
    call lba_to_chs
    mov ax, [root_dir_size]
    xor bx, bx
    mov es, bx
    mov bx, FAT_ROOT_ENTRIES
    call read_disk_chs
    jc error.disk_read

    ; Search for kernel
    xor ax, ax
    mov di, FAT_ROOT_ENTRIES
kernel_search:
    mov si, kernel_file_name
    mov cx, 11
    push di
    repe cmpsb
    pop di
    je found_kernel
    
    add di, 32
    inc ax
    cmp ax, [bpb.root_dir_entries]
    jb kernel_search

    jmp error.kernel_not_found

found_kernel:
    mov di, [di + FAT_CLUSTER_LOW]

    ; Calculate start sector for file data
    mov ax, [bpb.reserved_sects]
    add ax, [fats_size]
    add ax, [root_dir_size]
    inc ax
    mov [start_sector], ax

    mov bx, KERNEL
    xor cx, cx
    mov cl, [bpb.sects_per_cluster]
    mov si, cx
read_kernel:
    ; Calculate sector for cluster
    ; sector = (cluster - 2) * sectors / cluster + start sector
    mov ax, di
    sub ax, 2
    mul si
    add ax, [start_sector]

    ; Read cluster
    call lba_to_chs
    mov ax, si
    call read_disk_chs
    jc error.disk_read

    ; Advance kernel pointer
    mov ax, SECTOR_SIZE
    mul si
    add bx, ax

    ; Get next cluster
    shl di, 1
    mov di, [FAT + di]
    cmp di, 0xFFF8
    jb read_kernel
    
    mov ax, [start_sector]
    jmp KERNEL

error:
    .disk_read_msg: db 'Boot loader error: disk status '
    .disk_read_msg_len = $ - .disk_read_msg
    .disk_read:
    push ax
    mov ax, .disk_read_msg_len
    mov bx, .disk_read_msg
    call print_string
    pop ax
    shr ax, 8
    call print_int
    call println
    jmp .hault

    .kernel_msg: db 'Boot loader error: kernel not found', 13, 10
    .kernel_msg_len = $ - .kernel_msg
    .kernel_not_found:
    mov ax, .kernel_msg_len
    mov bx, .kernel_msg
    call print_string
    jmp .hault

    .hault:
    hlt
    jmp .hault