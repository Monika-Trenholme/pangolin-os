PROGRAMS = $(patsubst src/programs/%.asm,%,$(wildcard src/programs/*.asm))

bin/%: src/boot/%.asm src/boot_common.asm src/boot_common.inc
	@mkdir -p bin
	fasm $< $@

bin/kernel: src/kernel/*.asm src/boot_common.asm
	@mkdir -p bin
	fasm src/kernel/main.asm $@

bin/%: src/programs/%.asm src/kernel_api.inc
	@mkdir -p bin
	fasm $< $@

img/fat.img: bin/boot_loader bin/kernel $(patsubst %,bin/%,${PROGRAMS}) readme.md
	@mkdir -p img
	dd if=/dev/zero of=$@ bs=512 count=65535
	mkfs.fat -F 16 -S 512 -s 4 -R 2 -r 512 -n "PANGOLIN OS" $@
	dd if=bin/boot_loader of=$@ bs=1 skip=60 seek=60 conv=notrunc
	mcopy -i $@ bin/kernel "::kernel"
	for bin in ${PROGRAMS}; do mcopy -i $@ "bin/$${bin}" "::$${bin}"; done
	mcopy -i $@ readme.md "::readme.md"

img/pangolin-os.img: bin/boot_sector img/fat.img
	dd if=bin/boot_sector of=$@ bs=512 count=1 conv=notrunc
	dd if=img/fat.img of=$@ bs=512 seek=1 count=65535 conv=notrunc

img: img/pangolin-os.img

qemu: img/pangolin-os.img
	qemu-system-x86_64 -drive format=raw,file=$<

clean:
	rm -rf img bin

.PHONY: img qemu clean